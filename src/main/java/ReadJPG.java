import org.omg.CosNaming.NamingContextPackage.NotFound;

import java.io.*;

public class ReadJPG implements FileParser {
    private String name = "jpg";

    public ReadJPG() {
    }

    public String getName() {
        return name;
    }

    public boolean checkFile(File file) throws NotFound {

        StringBuilder sb = new StringBuilder();
        byte[] fileData = new byte[(int) file.length()];

        try (DataInputStream dis = new DataInputStream(new FileInputStream(file))) {
            dis.readFully(fileData);

            String start = sb.append(Integer.toHexString(Byte.toUnsignedInt(fileData[0])))
                    .append(Integer.toHexString(Byte.toUnsignedInt(fileData[1]))).toString();
            sb.delete(0, 99);
            String end = sb.append(Integer.toHexString(Byte.toUnsignedInt(fileData[fileData.length - 2])))
                    .append(Integer.toHexString(Byte.toUnsignedInt(fileData[fileData.length - 1]))).toString();
            return start.equals("ffd8") && end.equals("ffd9");

        } catch (IOException e) {
            System.out.println("No such file");
            throw new NotFound();
        }
    }
}
