import org.omg.CosNaming.NamingContextPackage.NotFound;

import java.io.File;

public class Main {
    public static void main(String[] args) throws NotFound {

        File file = new File("example.gif");
        FileDeferentiator fileDeferentiator = new FileDeferentiator(file);
        System.out.println(fileDeferentiator.checkFile());
    }
}
