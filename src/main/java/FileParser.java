import org.omg.CosNaming.NamingContextPackage.NotFound;

import java.io.File;

public interface FileParser {
    boolean checkFile(File file) throws NotFound;
    String getName();
}
