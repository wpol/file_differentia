import org.omg.CosNaming.NamingContextPackage.NotFound;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ReadGIF implements FileParser {
    private String name = "gif";

    public ReadGIF() {
    }

    public String getName() {
        return name;
    }

    public boolean checkFile(File file) throws NotFound {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader bf = new BufferedReader(new FileReader(file))) {
            String result = sb.append(bf.readLine()).substring(0, 6);
            if (result.equals("GIF87a") || result.equals("GIF89a")) {
                return true;
            } else return false;

        } catch (IOException e) {
            System.out.println("No such file");
            throw new NotFound();
        }
    }
}
