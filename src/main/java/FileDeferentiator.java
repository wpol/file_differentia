import org.omg.CosNaming.NamingContextPackage.NotFound;

import java.io.File;
import java.util.List;
import java.util.Optional;

public class FileDeferentiator {
    private List<FileParser> readerlist = new FileReaders().createFileReaders();
    private File file;

    public FileDeferentiator(File file) {
        this.file = file;
    }

    public String checkFile() throws NotFound {
        boolean isHandled = false;
        String validExtension = "";
        String currentExtension = getExtension(file.getName());
        hasFileExtension(currentExtension);
        for (FileParser fileParser : readerlist) {
            if (fileParser.checkFile(file)) {
                validExtension = fileParser.getName();
                isHandled = true;
            }
        }
        isFileHandled(isHandled);
        if (currentExtension.equals(validExtension)){
            return "Extension is true";
        }else return "Extension is '" + currentExtension + "' while actually it's '" + validExtension + "'";
    }

    private void isFileHandled(boolean isHandled) {
        if (!isHandled) try {
            throw new ExtensionNotHandled("This extension isn't handled");
        } catch (ExtensionNotHandled extensionNotHandled) {
            extensionNotHandled.printStackTrace();
        }
    }

    private void hasFileExtension(String extension) {
        if (extension.equals("File has no extension")) try {
            throw new ExtensionNotFoundException("File has no extension");
        } catch (ExtensionNotFoundException e) {
            e.printStackTrace();
        }
    }

    private String getExtension(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1)).orElse("File has no extension");
    }
}
