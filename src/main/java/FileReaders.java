import java.util.ArrayList;
import java.util.List;

public class FileReaders {
    private ReadGIF readGIF = new ReadGIF();
    private ReadJPG readJPG = new ReadJPG();
    List<FileParser> readerList = new ArrayList<>();

    public List<FileParser> createFileReaders() {
        readerList.add(readGIF);
        readerList.add(readJPG);
        return readerList;
    }
}
