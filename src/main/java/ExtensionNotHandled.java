public class ExtensionNotHandled extends Exception {
    public ExtensionNotHandled(String message) {
        super(message);
    }
}
