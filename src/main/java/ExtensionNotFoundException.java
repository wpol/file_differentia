public class ExtensionNotFoundException extends Exception{

    public ExtensionNotFoundException(String message) {
        super(message);
    }
}
